import pyqtgraph as pg
import pyqtgraph.exporters
import numpy as np
import pandas as pd
from datatest import * 

pg.setConfigOption('background', 'w')
read = main()
read.readfile("testfile.xlsx")
i = read.getdata(['State','City'],['Shipping Cost'],'mean')

# print(i)
x = [[*x] for x in i.values()]
for x1 in x[0]:
        r = str(x1)
        print(r)
y = [y.values() for y in i.values()][0]
y1 = [y1 for y1 in y]

#----------------------------------------------------------------------
# self.plotLayout = QtWidgets.QVBoxLayout()
# self.tab_Chart = QtWidgets.QWidget()
# self.tab_Chart.setObjectName("tab_Chart")
# self.widget_2 = pg.PlotWidget(name='Plot')
# self.widget_2.setBackground('w')
# self.plotLayout.addWidget(self.widget_2)
# self.tab_Chart.setLayout(self.plotLayout)
# self.widget_2 = QtWidgets.QWidget(self.tab_Chart)
# self.widget_2.setGeometry(QtCore.QRect(0, 0, 631, 541))
# self.widget_2.setObjectName("widget_2")
def testchart(self):
        #must call dataframe to use .indexfd
        
        xja = self.listWidget_X.item(0).text()
        yja = self.listWidget_Y.item(0).text()
        w = self.getdata([xja],[yja],'mean')
        # print(w)
        df = pd.DataFrame(w)
        q = df.index.values
        q = list(enumerate(q))
        p = df[yja]
        fact = df.loc[:,p]
        yMin = 0
        yMax = 0
        Xmin = df.shape[0]
        if Xmin > 20:
            Xmin = 20
        for m in zip(fact):
            eachX = df.shape[0]
            eachY = fact[m]
            maxY = np.max(eachY)
            minY = np.min(eachY)
            if maxY >  yMax:
                yMax = maxY
            if minY < yMin:
                yMin = minY
            testchart = pg.BarGraphItem(x=np.arange(eachX), height=eachY, width=0.5, brush='red')
            self.widget_2.addItem(testchart)
            self.widget_2.plot(name=m, pen='red')
        
        # print(q)
        # df = pd.DataFrame(data=)
        # selectedItem = self.listWidget_filter.currentItem().text()