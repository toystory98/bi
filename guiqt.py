# Software 2
# 6001012620076 Chaninat Boonsiri
# 6001012630021 Chanakan Thimkham
# Update: 2/4/2019

from PyQt5 import QtCore, QtGui, QtWidgets
import sys
import pyqtgraph as pg
import numpy as np
import random
import dill
from datatest import *


class Ui_MainWindow(main):
    def __init__(self):
        main.__init__(self)
        self.legend = None
        # self.listfil = []
        self.data = {}

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1202, 702)
        # MainWindow.setStyleSheet("background-color:#cff1ec;")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        # self.centralwidget.setStyleSheet("background-color:#cff1ec;")

        # Dimention list widget
        self.listWidget_dimen = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget_dimen.setGeometry(QtCore.QRect(10, 30, 181, 181))
        self.listWidget_dimen.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.listWidget_dimen.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.listWidget_dimen.setObjectName("listWidget_dimen")

        # Measurement list widget
        self.listWidget_fact = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget_fact.setGeometry(QtCore.QRect(200, 30, 181, 181))
        self.listWidget_fact.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.listWidget_fact.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.listWidget_fact.setObjectName("listWidget_fact")

        # Horiziontal list widget
        self.listWidget_X = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget_X.setGeometry(QtCore.QRect(10, 240, 181, 181))
        self.listWidget_X.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.listWidget_X.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.listWidget_X.currentItemChanged.connect(self.showfilter)
        self.listWidget_X.itemClicked.connect(self.showfilter)
        self.listWidget_X.setObjectName("listWidget_X")

        # Vartical list widget
        self.listWidget_Y = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget_Y.setGeometry(QtCore.QRect(200, 240, 181, 181))
        self.listWidget_Y.setDefaultDropAction(QtCore.Qt.MoveAction)
        self.listWidget_Y.setDragDropMode(QtWidgets.QAbstractItemView.DragDrop)
        self.listWidget_Y.setObjectName("listWidget_Y")

        # filter list widget
        self.listWidget_filter = QtWidgets.QListWidget(self.centralwidget)
        self.listWidget_filter.setGeometry(QtCore.QRect(10, 450, 371, 131))
        self.listWidget_filter.itemActivated.connect(self.selectcheckbox)
        # self.listWidget_filter.itemActivated.connect(self.showfilter)
        self.listWidget_filter.setObjectName("listWidget_filter")

        # Tab widget chart
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(390, 10, 631, 571))
        self.tabWidget.setObjectName("tabWidget")
        self.tab_Chart = QtWidgets.QWidget()
        self.tab_Chart.setObjectName("tab_Chart")
        self.plotLayout = QtWidgets.QVBoxLayout()
        self.chart_Widget = pg.PlotWidget(name='Plot')
        self.chart_Widget.setBackground('w')
        self.plotLayout.addWidget(self.chart_Widget)
        self.tab_Chart.setLayout(self.plotLayout)
        
        self.tabWidget.addTab(self.tab_Chart, "")

        #Tab widget table
        self.tab_Table = QtWidgets.QWidget()
        self.tab_Table.setObjectName("tab_Table")
        self.tableLayout = QtWidgets.QVBoxLayout()
        self.widget = QtWidgets.QWidget(self.tab_Table)
        self.widget.setGeometry(QtCore.QRect(0, 0, 631, 541))
        self.widget = pg.TableWidget()
        self.tableLayout.addWidget(self.widget)
        self.tab_Table.setLayout(self.tableLayout)
        self.widget.setObjectName("widget")
        self.tabWidget.addTab(self.tab_Table, "")

        # Set label 
        self.label_dimen = QtWidgets.QLabel(self.centralwidget)
        self.label_dimen.setGeometry(QtCore.QRect(10, 10, 181, 16))
        self.label_dimen.setObjectName("label_dimen")
        self.label_fact = QtWidgets.QLabel(self.centralwidget)
        self.label_fact.setGeometry(QtCore.QRect(200, 10, 181, 16))
        self.label_fact.setObjectName("label_fact")
        self.label_X = QtWidgets.QLabel(self.centralwidget)
        self.label_X.setGeometry(QtCore.QRect(10, 220, 181, 16))
        self.label_X.setObjectName("label_X")
        self.label_Y = QtWidgets.QLabel(self.centralwidget)
        self.label_Y.setGeometry(QtCore.QRect(200, 220, 181, 16))
        self.label_Y.setObjectName("label_Y")
        self.label_filter = QtWidgets.QLabel(self.centralwidget)
        self.label_filter.setGeometry(QtCore.QRect(10, 430, 371, 16))
        self.label_filter.setObjectName("label_filter")

        # Chart type button
        self.Line_Button = QtWidgets.QToolButton(self.centralwidget)
        self.Line_Button.setGeometry(QtCore.QRect(1040, 100, 141, 121))
        self.Line_Button.setObjectName("Line_Button")
        self.Line_Button.clicked.connect(self.plotline)

        self.Bar_Button = QtWidgets.QToolButton(self.centralwidget)
        self.Bar_Button.setGeometry(QtCore.QRect(1040, 270, 141, 121))
        self.Bar_Button.setObjectName("Bar_Button")
        self.Bar_Button.clicked.connect(self.plotbar)

        # Set sum or mean for plot
        self.radio_Sum = QtWidgets.QRadioButton(self.centralwidget)
        self.radio_Sum.setGeometry(QtCore.QRect(1050, 430, 82, 17))
        self.radio_Sum.setObjectName("radio_Sum")
        self.radio_Sum.clicked.connect(self.selectradio)
        self.radio_mean = QtWidgets.QRadioButton(self.centralwidget)
        self.radio_mean.setEnabled(True)
        self.radio_mean.setGeometry(QtCore.QRect(1050, 460, 82, 17))
        self.radio_mean.setObjectName("radio_mean")
        self.radio_mean.clicked.connect(self.selectradio)
        
        # Set tab menu bar
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1202, 21))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        MainWindow.setMenuBar(self.menubar)

        # Satatus bar
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        # Add file to statusbar
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuFile.setTitle("File")

        self.actionOpen_File = QtWidgets.QAction(MainWindow)
        self.actionOpen_File.setObjectName("actionOpen_File")
        self.actionOpen_File.setShortcut("Ctrl+O")
        self.actionOpen_File.triggered.connect(self.openfile)

        self.actionSave_File = QtWidgets.QAction(MainWindow)
        self.actionSave_File.setObjectName("actionSave_File")
        self.actionSave_File.setText("Save")
        self.actionSave_File.setShortcut("Ctrl+S")
        self.actionSave_File.triggered.connect(self.SaveData)

        self.menuFile.addAction(self.actionOpen_File)
        self.menuFile.addAction(self.actionSave_File)
        self.menubar.addAction(self.menuFile.menuAction())

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_Chart), _translate("MainWindow", "Chart"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_Table), _translate("MainWindow", "Table"))
        self.label_dimen.setText(_translate("MainWindow", "Dimension"))
        self.label_fact.setText(_translate("MainWindow", "Measurement"))
        self.label_X.setText(_translate("MainWindow", "Horiziontal (X)"))
        self.label_Y.setText(_translate("MainWindow", "Vertical (Y)"))
        self.label_filter.setText(_translate("MainWindow", "Filter"))
        self.Line_Button.setText(_translate("MainWindow", "LINE"))
        self.radio_Sum.setText(_translate("MainWindow", "SUM"))
        self.radio_mean.setText(_translate("MainWindow", "MEAN"))
        self.Bar_Button.setText(_translate("MainWindow", "BAR"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.actionOpen_File.setText(_translate("MainWindow", "Open File"))

#----- add to list widget ----------------------------------
    def addlistwidg(self, fileobject, widget):
        for eachfo in fileobject:
            self.item = QtWidgets.QListWidgetItem(eachfo)
            widget.addItem(self.item)

#-------------------------------------------------------------
    def file_open(self, filepath):
        '''Load file.'''
        with open(filepath, 'rb') as file:
            self.data = dill.load(file)

#-------- open file on menu bar ----------------------------
    def openfile(self):
        global data_raw     #set global variable for use another function
        fileo = QtWidgets.QFileDialog.getOpenFileName()
        fileo = str(fileo[0])
        if fileo != '':
            filename = fileo.split('/')[-1]
            extension = filename.split('.')[-1]
            self.listWidget_dimen.clear()
            self.listWidget_fact.clear()
            self.listWidget_X.clear()
            self.listWidget_Y.clear()
            self.listWidget_filter.clear()
    

            if extension == 'xlsx':
                self.readfile(fileo)     #create first data frame as data_raw
                data_raw = self.datamain()  #set variable correct original dataframe 
                self.addlistwidg(self.headDimen(), self.listWidget_dimen)
                self.addlistwidg(self.headFact(), self.listWidget_fact)

            elif extension == 'demobi':
                self.file_open(filename)
                self.addlistwidg(self.data['listdimen'], self.listWidget_dimen)
                self.addlistwidg(self.data['listfact'], self.listWidget_fact)
                self.addlistwidg(self.data['listXbox'], self.listWidget_X)
                self.addlistwidg(self.data['listYbox'], self.listWidget_Y)

#---------------------------------------------------------------
    def SaveData(self):
        filename = QtWidgets.QFileDialog.getSaveFileName()
        filename = filename[0]
        self.dataforplot()
        
        if filename != '':
            filename = filename.split('/')[-1] + '.demobi'
            self.Savefile(filename, self.data)

#--------------------------------------------------------------------
    def dataforplot(self):
        # self.ldimen = list()
        self.data['listdimen'] = list()
        for eachdata in range(self.listWidget_dimen.count()):
            self.data['listdimen'].append(self.listWidget_dimen.item(eachdata).text())

        # self.lfact = list()
        self.data['listfact'] = list()
        for eachdata in range(self.listWidget_fact.count()):
            self.data['listfact'].append(self.listWidget_fact.item(eachdata).text())
        print(self.data['listfact'])

        # self.lX = list()
        self.data['listXbox'] = list()
        for eachdata in range(self.listWidget_X.count()):
            self.data['listXbox'].append(self.listWidget_X.item(eachdata).text())
        # print(self.lX)
        
        # self.lY = list()
        self.data['listYbox'] = list()
        for eachdata in range(self.listWidget_Y.count()):
            self.data['listYbox'].append(self.listWidget_Y.item(eachdata).text())
  
#--------- test plot table [not finish yet]-----------------------
    def taBle(self,Y):

        for i in self.factline:
            eachY = list(self.df[i])

        data = {Y[a] : list([str(eachY[a])]) for a in range(len(self.factline))}
        # print(data)
        self.widget.setData(data)
        self.widget.setHorizontalHeaderLabels(self.data['listYbox'])

#-------update from check box filter------------------------------------
    def selectfilter(self):
        global data_raw
        date_col = self.columnDate()
        self.data['listfil'] = list()   #list keep filter that had check/
        colX = self.listWidget_X.item(self.listWidget_X.currentRow()).text()
        xbox = self.listWidget_X.item(0).text()
        ybox = self.listWidget_Y.item(0).text()

        for i in range(self.listWidget_filter.count()):   #range of all checked
            fill = self.listWidget_filter.item(i)   
            if fill.text() != 'Uncheck all' and fill.text() != 'Check all':
                if fill.checkState() == 2:      #if that filter had check
                    self.data['listfil'].append(fill.text())    #append to listfil

        if colX in ['Year','Month','Day','Quarter']:
            data_raw = self.FilDataDate(self.datamain(),colX,self.data['listfil']) 
        else:
            data_raw = self.datareal(self.datamain(),colX,self.data['listfil']) #filter data that will use
        listx =  self.data['listXbox']
        for i in range(len(listx)):
            if listx[i] in ['Year','Month','Day','Quarter']:
                listx[i] = date_col
        print(listx)
        data_dict = self.getdata(data_raw, listx,[ybox],self.selectradio())  #import data as dict that selected
        data_real = pd.DataFrame(data_dict)

        return data_real
  
#------uncheck or check all filter-----------------------------------------------
    def selectcheckbox(self):

        selectedItem = self.listWidget_filter.currentItem().text()

        if selectedItem == 'Uncheck all':
            for i in range(self.listWidget_filter.count()):
                itemfil = self.listWidget_filter.item(i)
                if itemfil.text() != 'Uncheck all' and itemfil.text() != 'Check all':
                    if itemfil.checkState() == 2:
                        itemfil.setCheckState(QtCore.Qt.Unchecked)

        elif selectedItem == 'Check all':
            for i in range(self.listWidget_filter.count()):
                itemfil = self.listWidget_filter.item(i)
                if itemfil.text() != 'Uncheck all' and itemfil.text() != 'Check all':
                    if itemfil.checkState() == 0:
                        itemfil.setCheckState(QtCore.Qt.Checked)
                        # print(itemfil.text())


#-----------test filter widget-------------------------------------------
    def showfilter(self):
        if self.listWidget_X.currentRow() != -1:
            column = self.listWidget_X.item(self.listWidget_X.currentRow()).text()
            if column == 'Year':
                callfilter = self.filterYear()
            elif column == 'Month':
                callfilter = self.filterMonth()
            elif column == 'Day':
                callfilter = self.filterDay()
            elif column == 'Quarter':
                callfilter = self.filterQuarter()
            else:
                callfilter = self.filterdata(column)                   #call func from datatest
            self.listWidget_filter.clear()
            self.listWidget_filter.addItem(QtWidgets.QListWidgetItem('Check all'))
            self.listWidget_filter.addItem(QtWidgets.QListWidgetItem('Uncheck all'))

            for i in callfilter:
                self.itemm = QtWidgets.QListWidgetItem()
                self.itemm.setText(i)
                self.itemm.setFlags(self.itemm.flags() | QtCore.Qt.ItemIsUserCheckable)
                # if self.itemm.text() not in self.listfil:
                self.itemm.setCheckState(QtCore.Qt.Checked)
                self.listWidget_filter.addItem(self.itemm)
                # else:
                #     self.itemm.setCheckState(QtCore.Qt.Unchecked)
                #     self.listWidget_filter.removeItemWidget(self.itemm)
        else:
            self.listWidget_filter.clear()

#-------------SELECT SUM OR MEAN------------------------------
    def selectradio(self):
        self.text = str()

        if self.radio_mean.isChecked():
            self.text = 'mean'
        elif self.radio_Sum.isChecked():
            self.text = 'sum'
        return self.text

#---------------RANDOM COLOR------------------------
    def randomcolor(self):
        listcolor = ['#b78338','bc5f6a','#034b61','#632a7e','#57233a','#a88661','#7e212c','#edbc7a','#847072','#007f52']
        realcolor = str(random.choice(listcolor))
        return realcolor

#--------for bar button------------------------------
    def plotbar(self):
        global data_raw
        self.dataforplot()
        if len(self.data['listXbox']) >= 1:
            self.ybox = self.listWidget_Y.item(0).text()
            self.updatedata = self.selectfilter()

            self.df = pd.DataFrame(self.updatedata)
            self.df = self.df.fillna(0)
            self.listdim = self.df.index.values
            
            self.factline = self.df.loc[:, [self.ybox]]
            dimline = list(map(str, self.listdim))
            dimline = list(enumerate(dimline))
            self.taBle(self.listdim)
            self.chart_Widget.showGrid(x=False, y=False)
            self.chart_Widget.clear()
            self.chart_Widget.clearPlots()

            if self.legend != None:
                self.legend.scene().removeItem(self.legend)
            self.legend = self.chart_Widget.addLegend()

            yMin = 0
            yMax = 0
            Xmin = self.df.shape[0]
            if Xmin > 20:
                Xmin = 20
            for eachMeasurement in self.factline:
                eachX = self.df.shape[0]
                eachY = self.df[eachMeasurement].astype(float)
                maxY = np.max(eachY)
                minY = np.min(eachY)
                if maxY >  yMax:
                    yMax = maxY
                if minY < yMin:
                    yMin = minY

                barChart = pg.BarGraphItem(x=np.arange(eachX), height=eachY, width=0.5, brush=self.randomcolor())
                self.chart_Widget.addItem(barChart)
                self.chart_Widget.plot(name=eachMeasurement, pen=self.randomcolor())

            self.chart_Widget.setLimits(yMin= yMin, yMax=yMax)
            self.chart_Widget.setXRange(0, Xmin)
            self.chart_Widget.getAxis('bottom').setTicks([dimline])
            data_raw = self.datamain()

#------------------------FOR LINE BUTTON-------------------------------------------------------
    def plotline(self):
        global data_raw
        
        self.dataforplot()
        if len(self.data['listXbox']) >= 1 :
            self.ybox = self.listWidget_Y.item(0).text()
            self.updatedata = self.selectfilter()

            self.df = pd.DataFrame(self.updatedata)
            self.df = self.df.fillna(0)
            self.listdim = self.df.index.values
            self.factline = self.df.loc[:, [self.ybox]]
            # print(self.listdim)
            dimline = list(map(str, self.listdim))
            dimline = list(enumerate(dimline))
            self.taBle(self.listdim)
            self.chart_Widget.showGrid(x=True, y=True)
            self.chart_Widget.clear()
            self.chart_Widget.clearPlots()

            if self.legend != None:
                self.legend.scene().removeItem(self.legend)
            self.legend = self.chart_Widget.addLegend()

            yMin = 0
            yMax = 0
            Xmin = self.df.shape[0]
            if Xmin > 20:
                Xmin = 20
            for eachMeasurement in self.factline:
                eachX = self.df.shape[0]
                eachY = list(self.df[eachMeasurement].astype(float))
                maxY = np.max(eachY)
                minY = np.min(eachY)
                if maxY >  yMax:
                    yMax = maxY
                if minY < yMin:
                    yMin = minY

                self.chart_Widget.plot(x=np.arange(eachX), y=eachY,pen=self.randomcolor(), symbol='+', symbolPen=self.randomcolor(), name=eachMeasurement)

            self.chart_Widget.setLimits(yMin= yMin, yMax=yMax)
            self.chart_Widget.setXRange(0, Xmin)
            self.chart_Widget.getAxis('bottom').setTicks([dimline])
            data_raw = self.datamain()
        

if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())