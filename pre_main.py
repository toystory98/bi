from classreadxlsx import * 
import pandas as pd
import numpy as np

# file = pd.read_excel("Mag6PlusEarthquakes_1900-2013.xlsx")
# file = pd.read_excel("faa_data_subset.xlsx")
# file = pd.read_excel("global_superstore_2016.xlsx")

def open(file):
    global data
    file = pd.read_excel(file)
    file = file.fillna(0)
    data = coldata()
    data.creatdic(file)
    return data

data = open("global_superstore_2016.xlsx")

def convertmonth(num):
    listm = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec']
    listnum = ['01','02','03','04','05','06','07','08','09','10','11','12']
    for i in range(len(listm)):
        if num == listnum[i]:
            num =listm[i]
    return num

def groupbymonth(data):
    monthfact = {'Jan':{},'Feb':{},'Mar':{},'Apr':{},'May':{},'Jun':{},'Jul':{},'Aug':{},'Sep':{},'Oct':{},'Nov':{},'Dec':{}}
    for h in data.header:
        if 'date' in h.lower():
            dt = h
    for c in range(len(data.dicdimen[dt])):
        month = convertmonth(data.dicdimen[dt][c][5:7])
        for h in data.fact:
            monthfact[month].setdefault(h, []).append(np.around(float(data.dicfact[h][c]), decimals=2))
    return monthfact

def groupbyday(data):
    dayfact = dict()
    for h in data.header:
        if 'date' in h.lower():
            dt = h
    for c in range(len(data.dicdimen[dt])):
        day = data.dicdimen[dt][c][8:10]
        dayfact.setdefault(day, {})
        for h in data.fact:
            dayfact[day].setdefault(h, []).append(float(data.dicfact[h][c]))
        dayfact = dict(sorted(dayfact.items()))
    return dayfact

def groupbyyear():
    yearfact = dict()
    for h in data.header:
        if 'date' in h.lower():
            dt = h
    for c in range(len(data.dicdimen[dt])):
        year = data.dicdimen[dt][c][:4]
        yearfact.setdefault(year, {})
        for h in data.fact:
            yearfact[year].setdefault(h, []).append(float(data.dicfact[h][c]))
        yearfact = dict(sorted(yearfact.items()))
    return yearfact


def selectdata(dim,fac,data):
    dicplot = dict()
    for h in dim:
        if data[h] != {}:
            dicplot[h] = data[h][fac]
        else:
            dicplot[h] = []
    return dicplot


a = groupbymonth(data)
#b = groupbyday(data)
# c = groupbyyear(data)
# print(a['Mar']['mag'])
# for i in [*a]:
#     print(i)
d = selectdata([*a],'Sales',a)
print(d)
# print(data.dicdimen['Date'][0][:4])
