import pandas as pd
import re

file = pd.read_excel("Mag6PlusEarthquakes_1900-2013.xlsx")
class coldata():
    def creatdic(self,file):
        self.header = list(file) #list all header
        self.dicdimen={}
        self.dicfact={}
        self.dicmain={}
        self.dimension=[]
        self.dicdate = {}
        self.fact=[] #eiei

        for i in self.header:
            data = str(file[i][0])
            a = re.search('[a-z,A-Z,:,/]',data)
            b = re.search('(id)|(code)|(lat)|(long)',i.lower())

            if a or b != None :
                self.dimension.append(i)
            else:
                self.fact.append(i)

        for i in self.dimension:
            col=[]
            for n in range(20):
                if 'date' in i.lower():
                    d = str(file[i][n])
                    col.append(d[:10])
                else:
                    col.append(str(file[i][n]))
                self.dicdimen[i] = col
        for i in self.fact:
            col=[]
            for n in range(20):
                col.append(str(file[i][n]))
                self.dicfact[i] = col
        self.dicmain['dimension']=self.dicdimen
        self.dicmain['fact']=self.dicfact
