#try to fetch data
import pandas as pd
import numpy as np
import re
import dill

def convertmonth(num):
    listm = ['January','February','March','April','May','June','July','August','September','October','November','December']
    listnum = ['01','02','03','04','05','06','07','08','09','10','11','12']
    for i in range(len(listm)):
        if num == listnum[i]:
            num =listm[i]
    return num

class main:
    def __init__(self):
        self.dimension = ['Year','Month','Day','Quarter']
        self.fact = []

    def readfile(self,filename):
        self.file = pd.read_excel(filename) #open file
        allhead = list(self.file)   #get all header column
        for i in allhead:
            data = str(self.file[i][0])     #check data in each column
            a = re.search('[a-z,A-Z,:,/]',data)     #check if it text it is dimension
            b = re.search('(id)|(code)|(lat)|(long)',i.lower())     #check dimension
            if a or b != None :
                self.dimension.append(i)
            else:
                self.fact.append(i)
        # self.file = self.file.fillna(0)

    def Savefile(self,filepath, data):
        with open(filepath, 'wb') as file:
            dill.dump(data, file)
    
    def datamain(self):     #get data from file as dataframe
        return self.file

    def headDimen(self):    #call header of dimension
        return self.dimension

    def headFact(self):     #call header of measurement(fact)
        return self.fact

#select data from Dimension and Measurement(fact) and put in dict_of_dict

    def getdata(self,df,dimension,fact,func):    #collect data that had filter to dict
        dicdata = dict()
        for d in self.dimension :
            if 'date' in d.lower():
                df[d]=df[d].astype(str)
        for i in fact:
            if func == 'mean':
                data = df.groupby(dimension).mean()[i].to_dict()
                dicdata[i] = data
            elif func == 'sum':
                data = df.groupby(dimension).sum()[i].to_dict()
                dicdata[i] = data
            
        return dicdata  #return data that as dict

    def filterdata(self,dimension): # filter data in selected dimension
        list_filter = []
        for i in list(self.datamain()[dimension].astype(str)):
            if i not in list_filter:
                list_filter.append(i)
        return list_filter

    def datareal(self,df,str_X,list_filter):    #dataframe of used data
        originfilter = self.filterdata(str_X)    #list filter in selected dimension
        filterpop = []  #list of filter not use

        for fil in originfilter: 
            if fil not in list_filter:
                filterpop.append(fil)
        
        if filterpop != []:     #if had filter data
            update = df[~df[str_X].isin(filterpop)]

        else:   #if not filter data
            update = df

        return update   #return as dataframe
    
    def FilDataDate(self,df,TypeOfDate,list_filter):    #filter data by Year Month Day or Quarter
        date = df[self.columnDate()]    #select column that it have date value
        list_seldate = []   #collect filter that had check
        if TypeOfDate == 'Year':
            for i in date:
                if str(i)[0:4] in list_filter:
                    list_seldate.append(i)
                    
        elif TypeOfDate == 'Month':
            for i in date:
                if convertmonth(str(i)[5:7]) in list_filter:
                    list_seldate.append(i)
                    
        elif TypeOfDate == 'Day':
            for i in date:
                if str(i)[8:10] in list_filter:
                    list_seldate.append(i)

        elif TypeOfDate == 'Quarter':
                l1 = list(df[self.columnDate()])
                l2 = list(df[self.columnDate()].dt.quarter)
                for i in range(len(l2)):
                    if str(l2[i]) in list_filter:
                        list_seldate.append(l1[i])
        
        data_select = df[df[self.columnDate()].isin(list_seldate)]  #filter dataframe by filter that had checked from list_seldate
        return data_select

    def columnDate(self):       #select column that have value as date
        for d in self.dimension :
            if 'date' in d.lower():
                coldate = d
        return coldate  #return the name of column
    
    def filterYear(self):   #get all year in data
        year = []
        allYear = list(self.file[self.columnDate()].dt.year)
        for y in allYear :
            if str(y) not in year:
                year.append(str(y))
        year.sort()
        return year

    def filterMonth(self):  #get all month in data
        month = []
        allMonth = list(self.file[self.columnDate()].dt.month_name())
        for m in allMonth :
            if str(m) not in month:
                month.append(str(m))
        month.sort()
        return month

    def filterDay(self):  #get all day in data
        day = []
        allDay = list(self.file[self.columnDate()].dt.day)
        for d in allDay :
            dd = str(d)
            if len(str(d)) == 1:
                dd = '0'+dd
                if dd not in day:
                    day.append(dd)
        day.sort(key = int) 
        return day

    def filterQuarter(self):     #get quarter in data
        quarter = []
        allQuarter = list(self.file[self.columnDate()].dt.quarter)
        for q in allQuarter :
            if str(q) not in quarter:
                quarter.append(str(q))
        quarter.sort()
        return quarter
    
# if __name__ == '__main__':
#     read = main()
#     read.readfile("testfile.xlsx")


